#ifndef RC4_H
# define RC4_H

int	rc4(char *key, int key_len, char *msg, int msg_len);

#endif
