SRCS = rc4.s
OBJS = rc4.o

NASM = nasm
RM = rm -rf

UNAME := $(shell uname)

ifeq ($(UNAME), Darwin)
	FORMAT = macho64
else
	FORMAT = elf64
endif

all:
	$(NASM) -f $(FORMAT) -o $(OBJS) $(SRCS)
	gcc main.c $(OBJS) -o rc4

