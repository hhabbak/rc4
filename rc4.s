default rel

section .data
    KEY     DQ 0x00
    KEY_LEN DQ 0x00
    MSG     DQ 0x00
    MSG_LEN DQ 0x00
    ARR_S TIMES 256 DB 0x00
    ARR_T TIMES 256 DB 0x00

section .text
    global rc4
    global _rc4

; rdi   rsi       rdx   rcx
; key   key_len   msg   msg_len
rc4:
_rc4:
    push rbp
    mov rbp, rsp
    sub rsp, 16

    mov rax, -1
    cmp rsi, 0
    je end

    mov [KEY], rdi
    mov [KEY_LEN], rsi
    mov [MSG], rdx
    mov [MSG_LEN], rcx
    mov rax, [KEY]
    call rc4_init
    call rc4_process
    xor rax, rax
end:
    leave
    ret

rc4_init:
    xor rcx, rcx
    lea rdx, [ARR_S]
rc4_init_t_loop:
    mov byte[rdx], cl
    call set_t

    inc rdx
    inc rcx
    cmp rcx, 256
    je rc4_init_s
    jmp rc4_init_t_loop
rc4_init_s:
    xor rcx, rcx
    xor rdx, rdx
rc4_init_s_loop:
    xor rbx, rbx
    lea rbx, [ARR_S]
    add rbx, rcx
    mov rbx, [rbx]
    add rdx, rbx ; += s[i]

    xor rbx, rbx
    lea rbx, [ARR_T]
    add rbx, rcx
    mov rbx, [rbx]
    add rdx, rbx ; += t[i]

    mov rax, rdx
    xor rdx, rdx
    mov rbx, 256
    div rbx ; rdx % 256

    call swap_s

    inc rcx
    cmp rcx, 256
    je rc4_init_end
    jmp rc4_init_s_loop
rc4_init_end:
    ret

; s[rcx] <-> s[rdx]
swap_s:
    lea rsi, [ARR_S]
    add rsi, rcx
    mov rax, [rsi]

    lea rdi, [ARR_S]
    add rdi, rdx
    mov rbx, [rdi]

    mov byte [rdi], al
    mov byte [rsi], bl
    ret

set_t:
    push rdx

    ; key[i  % key_len]
    xor rdx, rdx
    mov rax, rcx
    mov rbx, [KEY_LEN]
    div rbx ; rdx = i % key_len
    mov rax, [KEY]
    add rax, rdx

    ; t[i] = key[i % key_len]
    lea rbx, [ARR_T]
    add rbx, rcx
    mov rax, [rax]
    mov byte[rbx], al

    pop rdx
    ret

rc4_process:
    xor rcx, rcx
    xor rdx, rdx ; t1
    xor rbx, rbx ; t2
rc4_process_loop:
    push rbx
    inc rdx
    mov rax, rdx
    xor rdx, rdx
    mov rbx, 256
    div rbx ; t1 + 1 % 256
    pop rbx

    push rdx
    lea rdi, [ARR_S]
    add rdi, rdx
    xor rdx, rdx
    mov dl, byte [rdi]
    add rbx, rdx ; (t2 + s[t1])

    xor rdx, rdx
    mov rax, rbx
    mov rbx, 256
    div rbx
    mov rbx, rdx ; t2 % 256
    pop rdx

    push rcx
    push rbx
    mov rcx, rbx
    call swap_s
    pop rbx
    pop rcx

    xor rdi, rdi ; val
    lea rsi, [ARR_S]
    add rsi, rdx
    xor rax, rax
    mov al, byte[rsi]
    add rdi, rax

    lea rsi, [ARR_S]
    add rsi, rbx
    xor rax, rax
    mov al, byte[rsi]
    add rdi, rax ; val = s[t1] + s[t2]

    push rdx
    xor rdx, rdx
    mov rax, rdi
    mov rsi, 256
    div rsi
    mov rdi, rdx ; val % 256
    pop rdx

    lea rsi, [ARR_S]
    add rsi, rdi
    xor rdi, rdi
    mov dil, byte[rsi] ; val = t[val]

    mov rsi, [MSG]
    add rsi, rcx
    xor rax, rax
    mov al, byte[rsi]
    xor rdi, rax
    mov byte [rsi], dil

    inc rcx
    cmp rcx, [MSG_LEN]
    jl rc4_process_loop
    ret
