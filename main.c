#include <strings.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "rc4.h"

int main(int argc, char **argv)
{
	char *key;
	char *msg;
	int key_len;
	int msg_len;

	if (argc != 3)
		return -2;
	key = argv[1];
	msg = argv[2];
	key_len = strlen(key);
	msg_len = strlen(msg);
	rc4(key, key_len, msg, msg_len);
	write(1, msg, msg_len);
	/* rc4(key, key_len, msg, msg_len); */
	/* write(1, msg, msg_len); */
	return 0;
}
